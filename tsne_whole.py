from os import path

import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import umap
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt

font = {'size'   : 20}
plt.rc('font', **font)

import random
random.seed(777777)
np.random.seed(777777)

colour_list = ["xkcd:denim blue", "xkcd:pale red", "xkcd:grass green",
               "xkcd:orange", "xkcd:hot pink", "xkcd:cyan"]

method_labels = {"MuDeep": "Mudeep",
                 "ResNetMid": "ResNet50-MidFeat",
                 "MLFN": "MLFN",
                 "SENet": "S&E ResNet",
                 "OSNet": "OSNet x1 0",
                 "DenseNet": "DenseNet-121",
                 "InceptionV4": "Inception v4",
                 "ResNet": "ResNet-50",
                 "Xception": "Xception"}

def main(model, root_dir, data, method="tsne"):
    print("Starting", model)
    qf = np.load(path.join(root_dir, f"{data}_{model}_query_feats.npy"),
                    allow_pickle=True)
    q_pids = np.load(path.join(root_dir, f"{data}_{model}_query_pids.npy"),
                   allow_pickle=True)

    gf = np.load(path.join(root_dir, f"{data}_{model}_gallery_feats.npy"),
                    allow_pickle=True)
    g_pids = np.load(path.join(root_dir, f"{data}_{model}_gallery_pids.npy"),
                   allow_pickle=True)

    feats = np.concatenate((qf, gf), axis=0)
    pids = np.concatenate((q_pids, g_pids), axis=0)

    colours = [colour_list[pid] for pid in pids]

    if method == "tsne":
        tsne_feats = TSNE().fit_transform(feats)
    elif method == "umap":
        tsne_feats = umap.UMAP(n_neighbors=6,
                               metric='correlation').fit_transform(feats)
    else:
        ValueError(f"Unsupported dim reduction method: {method}")

    fig = plt.figure(figsize=(15,15))
    ax = fig.add_subplot(111)

    ax.scatter(tsne_feats[:,0], tsne_feats[:,1], c=colours)
    for pid, colour in zip(range(6), colour_list):
        ax.scatter([],[],c=colour,label=f"Pig {pid}",s=128)
    ax.legend(loc="upper left", prop={'size': 32})

    if method == "tsne":
        ax.set_xlim([-110,110]) # For t-SNE
        ax.set_ylim([-110,110]) # For t-SNE
    elif method == "umap":
        ax.set_xlim([-23,23]) # For umap
        ax.set_ylim([-23,23]) # For umap
    # plt.title(f"{method_labels[model]}")
    plt.tight_layout()
    plt.savefig(path.join(root_dir, f"{model}_2_whole_{method}.png"))
    plt.close()
    # plt.show()
    print("Done")

if __name__ == "__main__":
    import sys
    root_dir = sys.argv[1]
    data = sys.argv[2]
    reduc_method = sys.argv[3]
    if len(sys.argv[4:]) > 0:
        for model in sys.argv[4:]:
            main(model, root_dir, data, reduc_method)
    else:
        for model in ["DenseNet", "OSNet", "SENet"]:#, "MLFN", "MuDeep", "ResNetMid", "ResNet",
                    #  "InceptionV4", "Xception"]:
            main(model, root_dir, data, reduc_method)
