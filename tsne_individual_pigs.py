from os import path

import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import umap
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt
import seaborn as sns

font = {'size'   : 20}
plt.rc('font', **font)

import random
random.seed(666666)
np.random.seed(666666)


colour_list = ["b", "r", "g", "orange", "pink", "cyan"]

method_labels = {"MuDeep": "Mudeep",
                 "ResNetMid": "ResNet50-MidFeat",
                 "MLFN": "MLFN",
                 "SENet": "S&E ResNet",
                 "OSNet": "OSNet x1 0",
                 "DenseNet": "DenseNet-121",
                 "InceptionV4": "Inception v4",
                 "ResNet": "ResNet-50",
                 "Xception": "Xception"}

def main(model, root_dir, data):
    print("Starting", model)
    qf = np.load(path.join(root_dir, f"{data}_{model}_query_feats.npy"),
                    allow_pickle=True)
    q_pids = np.load(path.join(root_dir, f"{data}_{model}_query_pids.npy"),
                   allow_pickle=True)
    q_frames = np.load(path.join(root_dir, f"query_frames.npy"),
                   allow_pickle=True)

    gf = np.load(path.join(root_dir, f"{data}_{model}_gallery_feats.npy"),
                    allow_pickle=True)
    g_pids = np.load(path.join(root_dir, f"{data}_{model}_gallery_pids.npy"),
                   allow_pickle=True)
    g_frames = np.load(path.join(root_dir, f"gallery_frames.npy"),
                   allow_pickle=True)

    feats = np.concatenate((qf, gf), axis=0)
    pids = np.concatenate((q_pids, g_pids), axis=0)
    frames = np.concatenate((q_frames, g_frames), axis=0)

    # colours = [colour_list[pid] for pid in pids]
    print("Reducing dimensions")
    # tsne_feats = PCA(2).fit_transform(feats)
    # tsne_feats = TSNE(2).fit_transform(feats)
    tsne_feats = umap.UMAP(n_neighbors=6, metric='correlation').fit_transform(feats)

    palette = sns.cubehelix_palette(8, start=.5, rot=-.75, as_cmap=True)

    fig = plt.figure(figsize=(20,10))
    for pid in sorted(set(pids)):
        print(f"Plotting pig {str(pid)}")
        pid_feats = []
        pid_frames = []
        ax = fig.add_subplot(3,2,pid+1)

        for i in range(len(pids)):
            if pids[i] == pid:
                pid_feats.append(tsne_feats[i])
                pid_frames.append(frames[i])

        pid_feats = np.array(pid_feats)
        pid_frames = np.array(pid_frames)

        p = ax.scatter(pid_feats[:,0], pid_feats[:,1],
                       c=pid_frames, cmap=palette,
                       s=10)
        # ax.set_xlim([-110,110]) # For t-SNE
        # ax.set_ylim([-110,110]) # For t-SNE
        ax.set_xlim([-23,23]) # For umap
        ax.set_ylim([-23,23]) # For umap
        ax.set_title(f"Pig {pid}")

    plt.tight_layout()
    fig.subplots_adjust(right=0.8)

    cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
    cbar = plt.colorbar(cax=cbar_ax, mappable=p)

    cbar.set_ticks([pid_frames.min(), pid_frames.max()])
    cbar.set_ticklabels(["First frame", "Last frame"])

    # plt.savefig(path.join(root_dir, f"{model}_individual_tsne.png"))
    # plt.close()
    plt.show()
    print("Done")

if __name__ == "__main__":
    import sys
    root_dir = sys.argv[1]
    data = sys.argv[2]
    if len(sys.argv[3:]) > 0:
        for arg in sys.argv[3:]:
            main(arg, root_dir, data)
    else:
        for model in ["DenseNet", "OSNet", "SENet"]:#, "MLFN", "MuDeep", "ResNetMid", "ResNet",
                    #  "InceptionV4", "Xception"]:
            main(model, root_dir, data)
