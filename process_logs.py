import sys
from os import path, listdir
import re
from collections import OrderedDict

import matplotlib as mpl
mpl.use("agg")
from matplotlib import pyplot as plt
font = {'size'   : 22}
plt.rc('font', **font)


LOG_ROOT = "/home/jake/src/pig-reid/checkpoints/"
method_labels = {"mudeep": "Mudeep",
                 "hacnn": "HaCNN",
                 "resnet50mid": "ResNet50-MidFeat",
                 "mlfn": "MLFN",
                 "se_resnet50_fc512": "S&E ResNet",
                 "osnet_x1_0": "OSNet x1 0",
                 "densenet121_fc512": "DenseNet-121",
                 "inceptionv4": "Inception v4",
                 "resnet50_fc512": "ResNet-50",
                 "xception": "Xception"}



def extract_metric_value(line):
    return float(re.search(r"(\d*.\d*)%", line).group(1))/100

def get_rank(line):
    return re.search(r"Rank-(\d*)", line).group(1)

def get_model_name(line):
    return re.search(r"\/.*\/.*\/(.*)\/", line).group(1)

def plot_results(results, epochs, tag):
    fig = plt.figure(figsize=(20,10))
    metric_names = sorted(list(results.get(list(results.keys())[-1]).keys()))
    no_plots = len(metric_names)
    subplot_x = 3 if no_plots == 5 else 2
    subplot_y = 2 if no_plots == 5 else 2
    x_vals = list(range(0, epochs))

    lines = {}

    for i, metric in enumerate(metric_names):
        ax = fig.add_subplot(subplot_x,subplot_y,i+1)
        ax.set_title(metric)
        for model_name in sorted(results.keys()):
            print(metric, model_name)
            metric_values = results.get(model_name).get(metric)
            lines[model_name] = ax.plot(x_vals, metric_values,
                                        label=method_labels.get(model_name))
        ax.set_ylim([0,1])
        ax.set_xlabel("Epoch")
        ax.set_ylabel(metric)

    plt.legend(lines.values(),
               labels=[method_labels[method] for method in lines.keys()],
               loc="center right",
               borderaxespad=0.1,
               title="Models")

    plt.tight_layout()
    # plt.show()
    plt.savefig(f"{tag}_results.png")


def scrape_logs_from_tag(tag, epochs):
    tag_root = path.join(LOG_ROOT, tag)
    results = OrderedDict()
    for method_folder in sorted(listdir(tag_root)):
        if method_folder.endswith(".png"):
            continue
        method_log_file = path.join(tag_root, method_folder, "log.log")
        model_name = method_folder
        results[model_name] = {}

        with open(method_log_file, "r") as f:
            for line in f:
                if line.startswith("Setting save_dir"):
                    if not get_model_name(line) == model_name:
                        break
                if "mAP:" in line:
                    if not results[model_name].get("mAP"):
                        results[model_name]["mAP"] = []
                    results[model_name]["mAP"].append(
                                            extract_metric_value(line))
                if "Rank-" in line:
                    rank = get_rank(line)
                    if not results[model_name].get(f"rank_{rank}"):
                        results[model_name][f"rank_{rank}"] = []
                    results[model_name][f"rank_{rank}"].append(
                                            extract_metric_value(line))


        for metric in ["mAP", "rank_1", "rank_3", "rank_5"]:
            met_len = len(results[model_name][metric])
            missing_len = epochs-met_len
            results[model_name][metric] += [None]*missing_len

    return results

if __name__ == "__main__":
    tag = sys.argv[1]
    epochs = int(sys.argv[2])
    results = scrape_logs_from_tag(tag, epochs)
    import json
    plot_results(results, epochs, tag)
    mAP = []
    r1 = []
    r3 = []
    r5 = []
    models = []
    for model, metrics in results.items():
        models.append(method_labels.get(model))
        for metric, scores in metrics.items():
            if metric == "mAP":
                mAP.append(scores[-1])
            elif metric == "rank_1":
                r1.append(scores[-1])
            elif metric == "rank_3":
                r3.append(scores[-1])
            elif metric == "rank_5":
                r5.append(scores[-1])

    mAP = [round(x, 3) for x in mAP]
    r1 = [round(x, 3) for x in r1]
    r3 = [round(x, 3) for x in r3]
    r5 = [round(x, 3) for x in r5]

    from pandas import DataFrame
    import numpy as np
    df = DataFrame(np.array([models, mAP, r1, r3, r5]).transpose(),
                   columns=["Model", "mAP", "Rank-1", "Rank-3", "Rank-5"])
    df = df.sort_values(by="Model")
    print(df.to_latex(index=False))

