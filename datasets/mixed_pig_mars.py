from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import logging
from os import path, listdir

import numpy as np
from torchreid.data import VideoDataset

class MixedPigMars(VideoDataset):
    dataset_dir = "mixed_pig_mars_v2"
    def __init__(self, root='', **kwargs):
        self.root = root
        self.dataset_dir = path.join(self.root, self.dataset_dir)
        # self.cached_files = path.join(self.dataset_dir, "cache")

        train = self.load_data("train")
        query = self.load_data("query")
        gallery = self.load_data("gallery")

        print("Validating train")
        self.validate_dataset(train)
        print("Validating query")
        self.validate_dataset(query)
        print("Validating gallery")
        self.validate_dataset(gallery)

        super(MixedPigMars, self).__init__(train, query, gallery, **kwargs)

    def validate_dataset(self, dataset):
        count = 0
        for imgs, pid, cid in dataset:
            for img in imgs:
                img_fn = path.basename(img)
                try:
                    assert self._get_cam_id_from_name(img_fn) == cid
                    assert self._get_object_id_from_name(img_fn) == pid
                except AssertionError:
                    pass

    def load_data(self, dataset):
        return self.build_data(dataset)

    def build_data(self, dataset):
        if dataset == "train":
            return self._build_block("train")
        elif dataset == "query":
            test_block = np.array(self._build_block("test"))
            return self._build_query(test_block)
        elif dataset == "gallery":
            test_block = np.array(self._build_block("test"))
            return self._build_gallery(test_block)
        else:
            raise ValueError(f"Dataset {dataset} not defined")

    def _get_cam_id_from_name(self, filename):
        return int(filename.split("C")[1][0])

    def _get_object_id_from_name(self, filename):
        return int(filename.split("C")[0])

    def _all_same_cam(self, images):
        camid = self._get_cam_id_from_name(images[0])
        for image in images:
            if self._get_cam_id_from_name(image) != camid:
                return False
        return True

    def _build_block(self, dataset):
        tracklet_size = 10
        bbox = path.join(self.dataset_dir,
                         f"bbox_{dataset}")
        tracklets = []
        self.test_frame_order = []

        pids = sorted([int(x) for x in sorted(listdir(bbox))])
        pid2label = {pid:label for label, pid in enumerate(pids)}

        for pid in pids:
            logging.debug(f"Processing pid {pid}")
            pid_dir = path.join(bbox, str(pid).zfill(4))
            frames = sorted(listdir(pid_dir))
            for i in range(len(frames)):
                try:
                    selected_images = tuple(frames[i:i+tracklet_size])
                    selected_images = [path.join(self.dataset_dir,
                                                f"bbox_{dataset}",
                                                str(pid).zfill(4),
                                                img_fp) \
                                       for img_fp in selected_images]
                except IndexError:
                    break
                camid = self._get_cam_id_from_name(selected_images[0])
                if self._all_same_cam(selected_images):
                    tracklets.append((selected_images, pid2label[pid], camid))
                    if dataset == "test":
                        # Saved the frames used
                        self.test_frame_order.append(
                            int(frames[i].split("F")[1].split(".")[0])
                        )
        self.test_frame_order = np.array(self.test_frame_order)
        return tracklets

    def _load_query_idx(self):
        return np.load(path.join(self.dataset_dir, "info", "query_idx.npy"))

    def _build_query(self, test_block):
        query_idx = self._load_query_idx()
        np.save("/home/jake/data/saved_tensors/query_frames.npy",
                [self.test_frame_order[i] for i in query_idx])
        return test_block[query_idx,:]

    def _build_gallery(self, test_block):
        query_idx = self._load_query_idx()
        gallery_idx = [i for i in range(test_block.shape[0]) \
                       if i not in query_idx]
        np.save("/home/jake/data/saved_tensors/gallery_frames.npy",
                [self.test_frame_order[i] for i in gallery_idx])
        return test_block[gallery_idx,:]
