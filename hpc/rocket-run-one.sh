#!/bin/bash
# Tell SLURM which project's account to use:
#
#SBATCH -J pig-reid
#SBATCH --workdir=/nobackup/b1009723
#
#SBATCH -t 0-48:00:00
#
# Cores per task
#SBATCH -c 4
#
# number of nodes
#SBATCH -N 1
#
# use the power node
#SBATCH –p power
# how many GPUs
#SBATCH –gres=gpu:1   #Choose 0-4.
#
module load intel
#
# set the $OMP_NUM_THREADS variable
ompthreads=$SLURM_JOB_CPUS_PER_NODE
export OMP_NUM_THREADS=$ompthreads
#
conda activate reid
python /mnt/nfs/home/b1009723/src/reid/test_many.py $1 $2
