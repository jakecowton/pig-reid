import sys
from os import path
from datetime import datetime as dt
import argparse

import logging
import coloredlogs

from torch.nn import Linear, ModuleList, DataParallel
import torchreid
torchreid.utils.tools.set_random_seed(3333)
from torchreid.utils import Logger, count_num_param

from datasets.pig_mars import PigMars
from datasets.final_pig_mars import FinalPigMars
from datasets.mixed_pig_mars import MixedPigMars
torchreid.data.register_video_dataset('pig_mars', PigMars)
torchreid.data.register_video_dataset('final_pig_mars', FinalPigMars)
torchreid.data.register_video_dataset('mixed_pig_mars', MixedPigMars)

import socket
hostname = socket.gethostname()
if hostname == "cdial-compute" or hostname == "hulk":
    REID_DATA_PATH="/home/jake/data/reid-data"
elif hostname == "login02.cluster":
    REID_DATA_PATH="/nobackup/b1009723/reid-data"

OPEN_LAYER_MAP = {
        "densenet121_fc512": ["classifier", "fc"],
        "osnet_x1_0": ["classifier", "fc"],
        "mudeep": ["classifier", "fc"],
        "mlfn": ["classifier", "fc_x", "fc_s"],
        "resnet50mid": ["classifier", "fc_fusion"],
        "resnet50_fc512": ["classifier", "fc"],
        "inceptionv4": ["classifier"],
        "xception": ["classifier", "fc"],
        "se_resnet50_fc512": ["classifier", "fc"]
    }


def build_datamanager(data_type, sources, targets, height, width, batch_size,
                      seq_len, loss, sampler=None, num_instances=4, captum=False):
    print(f"Building the datamanager with:\nType: {data_type}\n"\
          f"Sources: {sources}\nheight: {height}\nwidth: {width}\n"\
          f"Batch size: {batch_size}\nSeq length: {seq_len}")

    if captum:
        return torchreid.data.VideoDataManager(
                    root=REID_DATA_PATH,
                    sources=sources,
                    height=height,
                    width=width,
                    combineall=False,
                    batch_size_train=batch_size,
                    seq_len=seq_len,
                    transforms='',
                    train_sampler="SequentialSampler",
                    sample_method="evenly",
                )

    if data_type == "image":
        _datamanager = torchreid.data.ImageDataManager

    elif data_type == "video":
        _datamanager = torchreid.data.VideoDataManager

    else:
        raise Exception(f"data_type must be 'image' or 'video', "\
                         "not {data_type}")

    datamanager = _datamanager(
                root=REID_DATA_PATH,
                sources=sources,
                targets=targets,
                height=height,
                width=width,
                combineall=False,
                batch_size_train=batch_size,
                train_sampler=sampler,
                num_instances=num_instances)

    return datamanager

def build_model(model_name, datamanager, model_loss, origin_size=None):
    print(f"Building model with:\nName: {model_name}\n"\
                 f"Loss: {model_loss}")
    model = torchreid.models.build_model(
                name=model_name,
                num_classes=datamanager.num_train_pids if not origin_size \
                            else origin_size,
                loss=model_loss,
                pretrained=True,
                use_gpu=True
            )
    if torchreid.utils.tools.torch.cuda.is_available() is True:
        print("Loading model onto GPU")
        model = model.cuda()
    return model

def build_optimiser(model, optim, lr, weight_decay=0.0005,
                    adam_beta1=0.9, adam_beta2=0.999):
    print("Setting optimiser")
    optimizer = torchreid.optim.build_optimizer(
                model, optim=optim, lr=lr, weight_decay=weight_decay,
                adam_beta1=adam_beta1, adam_beta2=adam_beta2
            )
    return optimizer

def build_scheduler(optimizer, epochs):
    print("Setting scheduler")
    scheduler = torchreid.optim.build_lr_scheduler(
                optimizer,
                lr_scheduler='cosine',
                max_epoch=epochs
            )
    return scheduler

def build_engine(model_loss, datamanager, model, optimizer, scheduler):
    print("Building engine")
    if model_loss == "softmax":
        engine = torchreid.engine.VideoSoftmaxEngine(
                    datamanager, model, optimizer, scheduler=scheduler,
                    pooling_method="avg"
                )
    elif model_loss == "triplet":
        engine = torchreid.engine.VideoTripletEngine(
                    datamanager, model, optimizer, scheduler=scheduler,
                    pooling_method="avg"
                )
    else:
        raise ValueError("model_loss must be 'triplet' or 'softmax', "\
                         f"not {model_loss}")
    return engine

def reconfigure_model(model, model_name, new_size):
    try:
        model.classifier = Linear(in_features=model.classifier.in_features,
                                  out_features=new_size,
                                  bias=True)
    except AttributeError:
        if model_name == "hacnn":

            model.classifier_local = \
                        Linear(in_features=model.classifier_local.in_features,
                               out_features=new_size,
                               bias=True)
            model.classifier_global = \
                        Linear(in_features=model.classifier_global.in_features,
                               out_features=new_size,
                               bias=True)
        elif model_name.startswith("pcb_p"):
            no_layers = int(model_name[-1])
            in_feat = model.classifier[0].in_features
            model.classifier = ModuleList([
                Linear(in_features=in_feat, out_features=new_size, bias=True),
                ] * no_layers)

    return model

def train_a_model(data_type, sources, targets, height, width, batch_size,
                  model_name, model_loss, optim, lr, resume,
                  resume_epoch, max_epoch, save_dir, seq_len=None,
                  load_dir=None, origin_size=None, freeze_epochs=None,
                  sampler=None, num_instances=None, test_only=False,
                  rerank=False, dist_metric="euclidean", visactmap=False,
                  captum_vis=False, captum_method="blended_heat_map", captum_sign="all"):

    if load_dir is not None:
        assert origin_size is not None, \
                "A load_dir requires a matching origin_size"

    sys.stdout = Logger(path.join(save_dir, "log.log"))

    print(f"Setting save_dir to {save_dir}")

    for arg, val in locals().items():
        print(f"{arg}: {val}")

    # Build the different components
    datamanager = build_datamanager(data_type, sources, targets, height, width,
                                    batch_size, seq_len, model_loss, sampler,
                                    num_instances, captum_vis)

    model = build_model(model_name, datamanager, model_loss, origin_size)

    optimizer = build_optimiser(model, optim, lr)
    scheduler = build_scheduler(optimizer, max_epoch)

    # Check for resume
    if resume is True:
        import torch
        print("Resuming checkpoint")

        if load_dir is None:
            chk = path.join(save_dir, f"model.pth.tar-{resume_epoch}")
        else:
            chk = path.join(load_dir, model_name, f"model.pth.tar-{resume_epoch}")
        print(f"Resuming from {chk}")
        use_cuda = True if torch.cuda.device_count() > 0 else False
        try:
            start_epoch, mAP, rank1 = torchreid.utils.resume_from_checkpoint(chk, model,
                                                            optimizer, use_cuda=use_cuda)
        except:
            start_epoch = torchreid.utils.resume_from_checkpoint(chk, model,
                                                            optimizer, use_cuda=use_cuda)
            mAP = 0
            rank1 = 0

        if load_dir :
            print("Reconfiguring the model")
            model = reconfigure_model(model, model_name,
                                      datamanager.num_train_pids)
            if use_cuda:
                model = model.cuda()

            optimizer = build_optimiser(model, optim, lr)
            scheduler = build_scheduler(optimizer, max_epoch)

    else:
        start_epoch = 0

    print(f"Model has {count_num_param(model)} params")

    print(f"Running engine with\nStart epoch:{start_epoch}\n"\
                 f"Max epoch: {max_epoch}")

    open_layers = OPEN_LAYER_MAP[model_name]

    if torch.cuda.device_count() > 1:
        model = DataParallel(model, device_ids=[0,1])

    engine = build_engine(model_loss, datamanager, model, optimizer, scheduler)

    if sources == "mars":
        engine.run(
                    start_epoch=start_epoch,
                    max_epoch=max_epoch,
                    save_dir=save_dir,
                    print_freq=100,
                    eval_freq=2,
                    open_layers=open_layers,
                    fixbase_epoch=freeze_epochs,
                    visrank=False,
                    visrank_topk=5,
                    ranks=[1,5,10],
                    test_only=test_only,
                    rerank=rerank,
                    dist_metric=dist_metric,
                    visactmap=visactmap,
                    captum_vis=captum_vis,
                    captum_method=captum_method,
                    captum_sign=captum_sign
                )
    elif "pig_mars" in sources:
        engine.run(
                    start_epoch=start_epoch,
                    max_epoch=max_epoch,
                    save_dir=save_dir,
                    print_freq=100,
                    eval_freq=1,
                    open_layers=open_layers,
                    fixbase_epoch=freeze_epochs,
                    visrank=False,
                    visrank_topk=5,
                    ranks=[1,3,5],
                    test_only=test_only,
                    rerank=rerank,
                    dist_metric=dist_metric,
                    visactmap=visactmap,
                    captum_vis=captum_vis,
                    captum_method=captum_method,
                    captum_sign=captum_sign
                )


def build_parser():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_type", dest="data_type",
                        default="video", type=str)
    parser.add_argument("--sources", dest="sources",
                        default="mars", type=str)
    parser.add_argument("--targets", dest="targets",
                        default="mars", type=str)
    parser.add_argument("--height", dest="height",
                        default=256, type=int)
    parser.add_argument("--width", dest="width",
                        default=128, type=int)
    parser.add_argument("--batch_size", dest="batch_size",
                        default=8, type=int)
    parser.add_argument("--model_name", dest="model_name",
                        required=True, type=str)
    parser.add_argument("--model_loss", dest="model_loss",
                        required=True, type=str)
    parser.add_argument("--optim", dest="optim",
                        default="adam", type=str)
    parser.add_argument("--lr", dest="lr",
                        default=0.001, type=float)
    parser.add_argument("--resume", dest="resume",
                        action="store_true")
    parser.add_argument("--resume_epoch", dest="resume_epoch",
                        type=int)
    parser.add_argument("--max_epoch", dest="max_epoch",
                        required=True, type=int)
    parser.add_argument("--load_dir", dest="load_dir",
                       type=str)
    parser.add_argument("--save_dir", dest="save_dir",
                        required=True, type=str)
    parser.add_argument("--seq_len", dest="seq_len",
                        default=10, type=int)
    parser.add_argument("--origin_size", dest="origin_size",
                        type=int)
    parser.add_argument("--freeze_epochs", dest="freeze_epochs",
                        default=0, type=int)
    parser.add_argument("--sampler", dest="sampler",
                        default=None, type=str)
    parser.add_argument("--num_instances", dest="num_instances",
                        default=None, type=int)
    parser.add_argument("--test_only", dest="test_only",
                        action="store_true")
    parser.add_argument("--rerank", dest="rerank",
                        action="store_true")
    parser.add_argument("--dist_metric", dest="dist_metric",
                        default="euclidean", type=str)
    parser.add_argument("--visactmap", dest="visactmap",
                        action="store_true")
    parser.add_argument("--captum_vis", dest="captum_vis",
                        action="store_true")
    parser.add_argument("--captum_method", dest="captum_method",
                        default="blended_heat_map", type=str)
    parser.add_argument("--captum_sign", dest="captum_sign",
                        default="all", type=str)
    return parser

if __name__ == "__main__":

    args = build_parser().parse_args()

    train_a_model(data_type=args.data_type,
                  sources=args.sources,
                  targets=args.targets,
                  height=args.height,
                  width=args.width,
                  batch_size=args.batch_size,
                  model_name=args.model_name,
                  model_loss=args.model_loss,
                  optim=args.optim,
                  lr=args.lr,
                  resume=args.resume,
                  resume_epoch=args.resume_epoch,
                  max_epoch=args.max_epoch,
                  load_dir=args.load_dir,
                  save_dir=f"./checkpoints/{args.save_dir}/{args.model_name}/",
                  seq_len=args.seq_len,
                  origin_size=args.origin_size,
                  freeze_epochs=args.freeze_epochs,
                  sampler=args.sampler,
                  num_instances=args.num_instances,
                  test_only=args.test_only,
                  rerank=args.rerank,
                  dist_metric=args.dist_metric,
                  visactmap=args.visactmap,
                  captum_vis=args.captum_vis,
                  captum_method=args.captum_method,
                  captum_sign=args.captum_sign)
