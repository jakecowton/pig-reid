from os import listdir, path
import json
from collections import OrderedDict

from matplotlib import pyplot as plt
font = {'size'   : 20}
plt.rc('font', **font)
import tensorflow as tf


method_labels = {"mudeep": "Mudeep",
                 "hacnn": "HaCNN",
                 "resnet50mid": "ResNet50-MidFeat",
                 "mlfn": "MLFN",
                 "se_resnet50_fc512": "S&E ResNet",
                 "osnet_x1_0": "OSNet x1 0",
                 "densenet121_fc512": "DenseNet-121",
                 "inceptionv4": "Inception v4",
                 "resnet50_fc512": "ResNet-50",
                 "xception": "Xception"}


class EventExtractor(object):
    def __init__(self, model_path):
        self.data = {}
        self.model_path = model_path
        tf_event_file = self.find_tf_file(model_path)
        self.iterate_summary(tf_event_file)
        with open(path.join(self.model_path, "tf_events.json"), "w") as f:
            json.dump(self.data, f)

    def find_tf_file(self, model_path):
        for f in listdir(model_path):
            if f.startswith("event"):
                return path.join(model_path, f)
        return None

    def iterate_summary(self, event_file):
        for summary in tf.train.summary_iterator(event_file):
            if summary.file_version == "":
                step = summary.step
                _dat = summary.summary.value[0]
                tag = _dat.tag.replace("Train/", "").lower()
                val = _dat.simple_value
                if not self.data.get(step):
                    self.data[step] = {}
                self.data[step][tag] = val

    def get_data(self, tag):
        return [self.data[step][tag] \
                for step in sorted(self.data.keys())]

    @property
    def acc(self):
        return self.get_data("acc")

    @property
    def loss(self):
        return self.get_data("loss")

    @property
    def time(self):
        return self.get_data("time")

    @property
    def steps(self):
        return sorted(self.data.keys())

def plot_models(models, root):
    fig = plt.figure(figsize=(20,10))
    lines = OrderedDict()

    for i, metric in enumerate(["acc", "loss"]):
        ax = fig.add_subplot(1,2,i+1)
        for model_name, ee in models.items():
            lines[model_name] = ax.plot(ee.steps, ee.get_data(metric),
                                        label=method_labels.get(model_name))
            ax.set_xlabel("Step")
            ax.set_ylabel("Accuracy" if metric == "acc" else "Loss")
        box = ax.get_position()

    fig.legend(lines.values(),
               labels=[method_labels[method] for method in lines.keys()],
               loc='lower center',
               bbox_to_anchor=(0.5, 0.12),
               ncol=3,
               title="Models")

    plt.tight_layout()
    plt.savefig(path.join(root, "train_acc_loss.png"))

if __name__ == "__main__":
    import sys
    root = sys.argv[1]
    models = OrderedDict()
    for model_name in sorted(listdir(root)):
        ee = EventExtractor(path.join(root, model_name))
        models[model_name] = ee
    plot_models(models, root)
